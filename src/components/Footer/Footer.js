/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './Footer.css';
import withViewport from '../../decorators/withViewport';
import withStyles from '../../decorators/withStyles';
import FaGithub from 'react-icons/lib/fa/github';
import Link from '../Link';

@withViewport
@withStyles(styles)
class Footer extends Component {

  static propTypes = {
    viewport: PropTypes.shape({
      width: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired,
    }).isRequired,
  };

  render() {
    // This is just an example how one can render CSS
    const { width, height } = this.props.viewport;
    this.renderCss(`.Footer-viewport:after {content:' ${width}x${height}';}`);

    return (
      <div className="Footer">
        <div className="Footer-container">
          <span className="Footer-text">Casualbot <FaGithub /></span>
          <span className="Footer-spacer"> | </span>
          <a className="Footer-text" href="https://github.com/CasualBot/react-twitbot/issues">Report an issue</a>
          <br/><br/>
          <span className="Footer-text">
            Disclaimer: This application will only get public user information and does not take responsibility for the end users actions. If you feel it is being abused please report it to Twitter here.
          </span>
        </div>
      </div>
    );
  }

}

export default Footer;
