import React, { Component } from 'react';
import styles from './Header.css';
import FaTwitter from 'react-icons/lib/fa/twitter';
import withStyles from '../../decorators/withStyles';



@withStyles(styles)
class Header extends Component {

  render() {
    return (
      <div className="Header">
        <div className="Header-container">
          <div className="Header-banner">
            <h1 className="Header-bannerTitle">Twitter Scraper  <FaTwitter /></h1>
            <p className="Header-bannerDesc">Easily scrape twitter with the click of a button</p>
          </div>
        </div>
      </div>
    );
  }

}

export default Header;
